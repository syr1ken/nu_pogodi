import * as Phaser from "phaser";

import SceneKeys from "../consts/SceneKeys";
import ScreenOverlay from "../game/ScreenOverlay";
import GameOver from "../game/GameOver";
import Score from "../game/Score";
import { GameOverSceneObject } from "../consts/interfaces";

export default class GameOverScene extends Phaser.Scene {
    private screen_overlay!: ScreenOverlay;
    private game_over_overlay!: GameOver;
    private score!: Score;

    constructor() {
        super(SceneKeys.GameOverScene);
    }

    init(data: GameOverSceneObject) {
        this.score = data.score;
    }

    create() {
        this.screen_overlay = new ScreenOverlay(this, {
            position: {
                x: 330,
                y: 3,
            },
        });

        this.screen_overlay.setVisibility(true);

        const game_over_text_style = {
            fontFamily: "interbold",
            fontSize: "58px",
            color: "#DDD",
            align: "center",
        };

        this.game_over_overlay = new GameOver(this, {
            position: {
                x: 371,
                y: 41,
            },
            score_text: {
                position: {
                    x: this.scale.width / 2,
                    y: this.scale.height / 2 - 100,
                },
                text: `СЧЕТ: ${this.score.score}`,
                style: {
                    text_style: game_over_text_style,
                    color: "#DDD",
                    hoverColor: "#FFF",
                    activeColor: "#BB1414",
                },
            },
            start_new_game_btn: {
                position: {
                    x: this.scale.width / 2,
                    y: this.scale.height / 2 + 0,
                },
                text: "НАЧАТЬ НОВУЮ ИГРУ",
                style: {
                    text_style: game_over_text_style,
                    color: "#DDD",
                    hoverColor: "#FFF",
                    activeColor: "#BB1414",
                },
            },
            go_to_main_menu_btn: {
                position: {
                    x: this.scale.width / 2,
                    y: this.scale.height / 2 + 100,
                },
                text: `ВЕРНУТЬСЯ В ГЛАВНОЕ МЕНЮ`,
                style: {
                    text_style: game_over_text_style,
                    color: "#DDD",
                    hoverColor: "#FFF",
                    activeColor: "#BB1414",
                },
            },
        });

        this.game_over_overlay.setVisibility(true);
    }

    update(t: number, dt: number): void {}
}
