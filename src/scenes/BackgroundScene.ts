import * as Phaser from "phaser";

import SceneKeys from "../consts/SceneKeys";
import TextureKeys from "../consts/TextureKeys";

export default class BackgroundScene extends Phaser.Scene {
    private background!: Phaser.GameObjects.Image;

    constructor() {
        super({ key: SceneKeys.BackgroundScene, active: true });
    }

    preload() {
        this.load.image(TextureKeys.background, "/assets/textures/x_800_envelop_background.svg");
    }

    create() {
        this.background = this.add.image(0, 0, TextureKeys.background).setOrigin(0);
    }

    update(t: number, dt: number): void {}
}
