import * as Phaser from "phaser";

import SceneKeys from "../consts/SceneKeys";
import TextureKeys from "../consts/TextureKeys";
import EmitKeys from "../consts/EmitKeys";
import FullScreenBtn from "../game/FullScreenBtn";
import VolumeBtn from "../game/VolumeBtn";

export default class UIScene extends Phaser.Scene {
    private top_left_button!: Phaser.GameObjects.Sprite;
    private bottom_left_button!: Phaser.GameObjects.Sprite;
    private top_right_button!: Phaser.GameObjects.Sprite;
    private bottom_right_button!: Phaser.GameObjects.Sprite;
    private menu_button_text!: Phaser.GameObjects.Text;
    private menu_button!: Phaser.GameObjects.Sprite;
    private full_screen_btn!: FullScreenBtn;
    private volume_btn!: VolumeBtn;
    
    constructor() {
        super({ key: SceneKeys.UIScene, active: true });
    }

    preload() {
        this.load.spritesheet(TextureKeys.button, "/assets/textures/x_800_envelop_button_sprite.svg", {
            frameWidth: 150,
            frameHeight: 150,
        });

        this.load.spritesheet(TextureKeys.long_button, "/assets/textures/x_800_envelop_long_button_sprite.svg", {
            frameWidth: 175,
            frameHeight: 77,
        });

        this.load.spritesheet(TextureKeys.full_screen_btn, "/assets/textures/x_800_envelop_full_screen_btn.svg", {
            frameWidth: 64,
            frameHeight: 64,
        });

        this.load.spritesheet(TextureKeys.volume_btn, "/assets/textures/x_800_envelop_volume_btn.svg", {
            frameWidth: 64,
            frameHeight: 64,
        });
    }

    create() {
        this.top_left_button = this.add.sprite(185, 493, TextureKeys.button).setInteractive();
        this.bottom_left_button = this.add.sprite(185, 675, TextureKeys.button).setInteractive();
        this.top_right_button = this.add.sprite(1585, 493, TextureKeys.button).setInteractive();
        this.bottom_right_button = this.add.sprite(1585, 675, TextureKeys.button).setInteractive();

        this.setUpControlButtonsEvents(this.top_left_button, EmitKeys.top_left_button_clicked);
        this.setUpControlButtonsEvents(this.bottom_left_button, EmitKeys.bottom_left_button_clicked);
        this.setUpControlButtonsEvents(this.top_right_button, EmitKeys.top_right_button_clicked);
        this.setUpControlButtonsEvents(this.bottom_right_button, EmitKeys.bottom_right_button_clicked);

        this.menu_button_text = this.add
            .text(185, 160, "МЕНЮ", {
                fontFamily: "interbold",
                fontSize: "52px",
                color: "#000",
                align: "center",
            })
            .setOrigin(0.5, 0.5);

        this.menu_button = this.add.sprite(185, 80, TextureKeys.long_button).setInteractive();

        this.setUpControlButtonsEvents(this.menu_button, EmitKeys.menu_button_clicked);

        this.full_screen_btn = new FullScreenBtn(this, {
            position: {
                x: 1550,
                y: 47,
            },
        });

        this.volume_btn = new VolumeBtn(this, {
            position: {
                x: 1550,
                y: 150,
            },
        });
    }

    update(t: number, dt: number): void {}

    setUpControlButtonsEvents(button: Phaser.GameObjects.Sprite, emit: string): void {
        const context = this;
        button.on("pointerdown", function (pointer: any) {
            button.setFrame(1);
            context.events.emit(emit)
        });
        button.on("pointerout", function (pointer: any) {
            button.setFrame(0);
        });
        button.on("pointerup", function (pointer: any) {
            button.setFrame(0);
        });
    }
}
