import * as Phaser from "phaser";
import { version } from "../../package.json";

import SceneKeys from "../consts/SceneKeys";
import StartGameMenu from "../game/StartGameMenu";
import ScreenOverlay from "../game/ScreenOverlay";

export default class StartGameMenuScene extends Phaser.Scene {
    private start_game_menu_overlay!: StartGameMenu;
    private screen_overlay!: ScreenOverlay;

    constructor() {
        super(SceneKeys.StartGameMenuScene);
    }

    create() {
        this.screen_overlay = new ScreenOverlay(this, {
            position: {
                x: 330,
                y: 3,
            },
        });

        this.screen_overlay.setVisibility(true);

        const start_game_menu_overlay_text_style = {
            fontFamily: "interbold",
            fontSize: "58px",
            color: "#DDD",
        };

        this.start_game_menu_overlay = new StartGameMenu(this, {
            position: {
                x: 371,
                y: 41,
            },
            start_new_game_btn: {
                position: {
                    x: this.scale.width / 2,
                    y: this.scale.height / 2,
                },
                text: "НАЧАТЬ НОВУЮ ИГРУ",
                style: {
                    text_style: start_game_menu_overlay_text_style,
                    color: "#DDD",
                    hoverColor: "#FFF",
                    activeColor: "#BB1414",
                },
            },
            version_text: {
                position: {
                    x: this.scale.width / 2,
                    y: this.scale.height / 2 + 370,
                },
                text: `ВЕРСИЯ ${version}`,
                style: {
                    text_style: {
                        ...start_game_menu_overlay_text_style,
                        fontSize: "20px",
                    },
                    color: "#DDD",
                    hoverColor: "#FFF",
                    activeColor: "#BB1414",
                },
            },
        });

        this.start_game_menu_overlay.setVisibility(true);
    }

    update(t: number, dt: number): void {}
}
