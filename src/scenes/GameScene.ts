import * as Phaser from "phaser";

import SceneKeys from "../consts/SceneKeys";
import TextureKeys from "../consts/TextureKeys";
import AudioKeys from "../consts/AudioKeys";
import PositionKeys from "../consts/PositionKeys";
import { HelperKeys } from "../consts/HelperKeys";
import EmitKeys from "../consts/EmitKeys";

import Volk from "../game/Volk";
import Egg from "../game/Egg";
import Score from "../game/Score";
import Difficulty from "../game/Difficulty";
import Zayac from "../game/Zayac";
import Chick from "../game/Chick";

import { EggObject, ChickObject } from "../consts/interfaces";

export default class GameScene extends Phaser.Scene {
    private volk!: Volk;
    private zayac!: Zayac;
    private sfx_egg_tick!: Phaser.Sound.BaseSound;
    private sfx_egg_broke!: Phaser.Sound.BaseSound;
    private sfx_egg_catch!: Phaser.Sound.BaseSound;
    private sfx_game_over!: Phaser.Sound.BaseSound;
    private egg_spawners: EggObject[] = [];
    private eggs!: Phaser.GameObjects.Group;
    private eggs_to_destroy: Phaser.GameObjects.GameObject[] = [];
    private egg_caught: boolean = false;
    private egg_broken: boolean = false;
    private game_loop_timer: number = 0;
    private game_loop_speed: number = 2000;
    private game_loop_speed_step: number = 200;
    private score!: Score;
    private difficulty!: Difficulty;
    private difficulty_divider: number = 50;
    private prev_difficulty_max_score: number = 0;
    private extra_health: number = 0;
    private chicks!: Phaser.GameObjects.Group;
    private chicks_to_destroy: Phaser.GameObjects.GameObject[] = [];
    private left_chick_spawner!: ChickObject;
    private right_chick_spawner!: ChickObject;
    private left_broken_egg!: Phaser.GameObjects.Sprite;
    private right_broken_egg!: Phaser.GameObjects.Sprite;
    private ui_scene!: Phaser.Scene;
    private game_over: Boolean = false;

    constructor() {
        super(SceneKeys.GameScene);
    }

    create() {
        this.game_over = false;
        this.eggs_to_destroy = [];
        this.chicks_to_destroy = [];

        this.sfx_egg_tick = this.sound.add(AudioKeys.egg_tick);
        this.sfx_egg_broke = this.sound.add(AudioKeys.egg_broke);
        this.sfx_egg_catch = this.sound.add(AudioKeys.egg_catch);
        this.sfx_game_over = this.sound.add(AudioKeys.game_over);

        this.input.once("pointerdown", (_: any) => {
            this.sound.unlock();
        });

        this.volk = new Volk(this, {
            position: {
                x: 899,
                y: 516,
            },
            frames: 4,
            health: {
                position: {
                    x: 1052,
                    y: 189,
                },
                current_health: 3,
                max_health: 3,
                frames: 4,
            },
        });

        this.add.existing(this.volk);

        this.egg_spawners = [
            {
                position: {
                    x: 530,
                    y: 319,
                },
                position_string: PositionKeys.top_left,
                broken_egg_position_string: PositionKeys.left,
                frames: 5,
                id: TextureKeys.egg_top_left,
            },
            {
                position: {
                    x: 527,
                    y: 505,
                },
                position_string: PositionKeys.bottom_left,
                broken_egg_position_string: PositionKeys.left,
                frames: 5,
                id: TextureKeys.egg_bottom_left,
            },
            {
                position: {
                    x: 1255,
                    y: 319,
                },
                position_string: PositionKeys.top_right,
                broken_egg_position_string: PositionKeys.right,
                frames: 5,
                id: TextureKeys.egg_top_right,
            },
            {
                position: {
                    x: 1261,
                    y: 500,
                },
                position_string: PositionKeys.bottom_right,
                broken_egg_position_string: PositionKeys.right,
                frames: 5,
                id: TextureKeys.egg_bottom_right,
            },
        ];

        this.score = new Score(this, {
            position: {
                x: 950,
                y: 30,
            },
            score: 0,
            style: {
                fontFamily: "digitalcyrillic",
                fontSize: "96px",
                color: "#000",
            },
        });

        this.game_loop_speed = 2000;

        this.eggs = new Phaser.GameObjects.Group(this);

        this.egg_caught = false;
        this.egg_broken = false;

        this.difficulty = new Difficulty(this, {
            difficulty: 1,
            min: 1,
            max: 7,
        });

        this.zayac = new Zayac(this, {
            position: {
                x: 595,
                y: 143,
            },
            show_ticks: 0,
        });

        this.zayac.zayac_sprite.visible = false;
        this.extra_health = 0;

        this.left_chick_spawner = {
            position: {
                x: 471,
                y: 662,
            },
            frames: 5,
            current_frame: 0,
            id: TextureKeys.chick_left,
        };

        this.right_chick_spawner = {
            position: {
                x: 1309,
                y: 662,
            },
            frames: 5,
            current_frame: 0,
            id: TextureKeys.chick_right,
        };

        this.chicks = new Phaser.GameObjects.Group(this);

        this.left_broken_egg = this.add.sprite(603, 724, TextureKeys.broken_egg);
        this.right_broken_egg = this.add.sprite(1185, 723, TextureKeys.broken_egg).setFlip(true, false);
        this.left_broken_egg.visible = false;
        this.right_broken_egg.visible = false;

        this.spawnEgg();

        this.ui_scene = this.scene.get(SceneKeys.UIScene);

        this.ui_scene.events.on(EmitKeys.top_left_button_clicked, (): void => {
            this.volk.turnTopLeft();
        });

        this.ui_scene.events.on(EmitKeys.bottom_left_button_clicked, (): void => {
            this.volk.turnBottomLeft();
        });

        this.ui_scene.events.on(EmitKeys.top_right_button_clicked, (): void => {
            this.volk.turnTopRight();
        });

        this.ui_scene.events.on(EmitKeys.bottom_right_button_clicked, (): void => {
            this.volk.turnBottomRight();
        });

        this.ui_scene.events.on(EmitKeys.menu_button_clicked, (): void => {
            if (this.scene.isPaused()) return;
            this.scene.pause();
            this.scene.bringToTop(SceneKeys.BackgroundScene);
            this.scene.bringToTop(SceneKeys.GamePauseScene);
            this.scene.bringToTop(SceneKeys.UIScene);
            this.scene.launch(SceneKeys.GamePauseScene, {
                difficulty: this.difficulty,
            });
        });

        this.events.on(HelperKeys.shutdown, () => {
            this.ui_scene.events.destroy();
        });

        this.events.on(
            HelperKeys.preupdate,
            () => {
                this.clearEggsToDestroyArray();
                this.clearChicksToDestroyArray();
            },
            this
        );
    }

    update(t: number, dt: number): void {
        this.resumeSceneSound();
        this.volk.inputHandler();

        if (t >= this.game_loop_timer) {
            this.game_loop_timer = t + this.game_loop_speed;
            this.gameLoopEvent();
        }
    }

    resumeSceneSound() {
        const sound = this.game.sound as Phaser.Sound.WebAudioSoundManager;
        if (sound?.context?.state && sound?.context?.state === HelperKeys.suspended) {
            sound.context.resume();
        }
    }

    gameLoopEvent(): void {
        this.egg_caught = false;
        this.egg_broken = false;
        this.left_broken_egg.visible = false;
        this.right_broken_egg.visible = false;
        this.eggsHandler();
        this.chicksHandler();
        if (this.getDifficultyRandomNum()) {
            this.spawnEgg();
        }
        this.difficultyHandler();
        this.zayacActivityHandler();
        this.handleSounds();
    }

    handleSounds() {
        if (this.game_over) {
            this.sfx_game_over.play();
            return;
        }

        if (this.egg_caught) {
            this.sfx_egg_catch.play();
            return;
        }

        if (this.egg_broken) {
            this.sfx_egg_broke.play();
            return;
        }

        if (this.eggs.children.entries.length) {
            this.sfx_egg_tick.play();
            return;
        }
    }

    spawnEgg(): void {
        const egg_spawner_idx = Phaser.Math.Between(0, 3);
        this.eggs.add(new Egg(this, this.egg_spawners[egg_spawner_idx]));
    }

    spawnChick(egg: Egg): void {
        if (egg.broken_egg_position === PositionKeys.left) {
            this.chicks.add(new Chick(this, this.left_chick_spawner));
        } else if (egg.broken_egg_position === PositionKeys.right) {
            this.chicks.add(new Chick(this, this.right_chick_spawner));
        }
    }

    addScore(): void {
        this.score.score = this.score.score + Egg.score_points * this.difficulty.getDifficulty;
        this.score.updateScoreText();
        this.registry.set(HelperKeys.score, this.score.score);
    }

    getDifficultyRandomNum(): boolean {
        if (this.difficulty.getDifficulty >= 3 && this.difficulty.getDifficulty <= 5) {
            return Phaser.Math.Between(1, 100) <= 70;
        } else if (this.difficulty.getDifficulty >= 6) {
            return Phaser.Math.Between(1, 100) <= 90;
        }
        return Phaser.Math.Between(1, 100) <= 50;
    }

    difficultyHandler(): void {
        const should_increase_difficulty: number = Math.floor(
            this.score.score /
                (this.difficulty_divider * this.difficulty.getDifficulty + this.prev_difficulty_max_score)
        );

        if (should_increase_difficulty === 0) {
            return;
        }

        if (this.difficulty.getDifficulty < this.difficulty.getMaxDifficulty) {
            this.prev_difficulty_max_score = this.score.score;
            this.difficulty.setDifficulty = this.difficulty.getDifficulty + 1;
            this.game_loop_speed = this.game_loop_speed - this.game_loop_speed_step;
        }
    }

    zayacActivityHandler(): void {
        if (this.zayac.getShowTicks > 0) {
            this.zayac.zayac_sprite.visible = true;
            this.zayac.subtractOneShowTick();
            return;
        }

        this.zayac.zayac_sprite.visible = false;
        this.extra_health = 0;

        if (Phaser.Math.Between(1, 100) <= 5) {
            this.zayac.setShowTicks = Phaser.Math.Between(5, 15);
            this.zayac.zayac_sprite.visible = true;
            this.extra_health = 1;
        }
    }

    eggsHandler(): void {
        this.eggs.children.iterate((child: Phaser.GameObjects.GameObject) => {
            if (!child) {
                return true;
            }

            const egg = child as Egg;

            if (egg.current_frame >= egg.frames - 1) {
                if (egg.egg_position === this.volk.volk_position) {
                    this.addScore();
                    this.egg_caught = true;
                } else {
                    if (this.zayac.zayac_sprite.visible && this.extra_health) {
                        this.extra_health--;
                        this.spawnChick(egg);
                    } else if (this.zayac.zayac_sprite.visible && !this.extra_health) {
                        this.extra_health++;
                        this.volk.subtractHealth(1);
                    } else {
                        this.volk.subtractHealth(1);
                    }

                    if (egg.broken_egg_position === PositionKeys.left) {
                        this.left_broken_egg.visible = true;
                    } else if (egg.broken_egg_position === PositionKeys.right) {
                        this.right_broken_egg.visible = true;
                    }

                    if (this.volk.health.current_health <= 0) {
                        this.game_over = true;
                        this.scene.start(SceneKeys.GameOverScene, {
                            score: this.score,
                        });
                        this.scene.bringToTop(SceneKeys.BackgroundScene);
                        this.scene.bringToTop(SceneKeys.GameOverScene);
                        this.scene.bringToTop(SceneKeys.UIScene);
                    }
                    this.egg_broken = true;
                }

                this.eggs_to_destroy.push(child);
            }

            if (egg.current_frame < egg.frames - 1) {
                egg.setNextFrame();
            }
            return true;
        });
    }

    clearEggsToDestroyArray() {
        for (let idx = 0; idx < this.eggs_to_destroy.length; idx++) {
            this.eggs_to_destroy[idx].destroy(true);
            this.eggs.children.delete(this.eggs_to_destroy[idx]);
        }
    }

    chicksHandler(): void {
        this.chicks.children.iterate((child: Phaser.GameObjects.GameObject) => {
            if (!child) {
                return true;
            }

            const chick = child as Chick;

            if (chick.current_frame >= chick.frames - 1) {
                this.chicks_to_destroy.push(child);
            }

            if (chick.current_frame < chick.frames - 1) {
                chick.setNextFrame();
            }

            return true;
        });
    }

    clearChicksToDestroyArray() {
        for (let idx = 0; idx < this.chicks_to_destroy.length; idx++) {
            this.chicks_to_destroy[idx].destroy(true);
            this.chicks.children.delete(this.chicks_to_destroy[idx]);
        }
    }
}
