import * as Phaser from "phaser";

import SceneKeys from "../consts/SceneKeys";
import TextureKeys from "../consts/TextureKeys";
import FullScreenBtn from "../game/FullScreenBtn";
import ScreenOverlay from "../game/ScreenOverlay";
import LettersTableInput from "../game/LettersTableInput";
import { HelperKeys } from "../consts/HelperKeys";

export default class Game extends Phaser.Scene {
    private screen_overlay!: ScreenOverlay;
    private letters_table_input!: LettersTableInput;

    constructor() {
        super(SceneKeys.AddNewRecord);
    }

    create() {
        const context = this;

        const background = this.add.image(0, 0, TextureKeys.background).setOrigin(0);

        const top_left_button = this.add.sprite(230, 493, TextureKeys.button);
        const bottom_left_button = this.add.sprite(230, 650, TextureKeys.button);
        const top_right_button = this.add.sprite(1539, 493, TextureKeys.button);
        const bottom_right_button = this.add.sprite(1539, 650, TextureKeys.button);

        const menu_button_text = this.add
            .text(1539, 200, "МЕНЮ", {
                fontFamily: "interbold",
                fontSize: "36px",
                color: "#000",
                align: "center",
            })
            .setOrigin(0.5, 0.5);

        const menu_button = this.add.sprite(1539, 150, TextureKeys.long_button);

        this.screen_overlay = new ScreenOverlay(this, {
            position: {
                x: 371,
                y: 41,
            },
        });

        this.screen_overlay.setVisibility(true);

        const record_table_header = this.add
            .text(this.scale.width / 2, 100, "ДОБАВИТЬ НОВЫЙ РЕКОРД", {
                fontFamily: "interbold",
                fontSize: "40px",
                color: "#DDD",
                align: "center",
            })
            .setOrigin(0.5, 0.5);

        const enter_name_subheader = this.add
            .text(this.scale.width / 2, 150, "ВВЕДИТЕ ИМЯ", {
                fontFamily: "interbold",
                fontSize: "16px",
                color: "#DDD",
                align: "center",
            })
            .setOrigin(0.5, 0.5);

        const letter_style = {
            fontFamily: "interbold",
            fontSize: "56px",
            color: "#DDD",
            align: "center",
        };

        const scene_x_center = this.scale.width / 2;
        const letter_y = 310;

        const letters_table_input_object = {
            position: {
                x: 371,
                y: 41,
            },
            text_field: {
                position: {
                    x: scene_x_center,
                    y: 210,
                },
                text: "",
                style: {
                    fontFamily: "interbold",
                    fontSize: "32px",
                    color: "#DDD",
                    align: "center",
                },
            },
            letters_list: [
                {
                    position: {
                        x: scene_x_center - 400,
                        y: letter_y,
                    },
                    text: "А",
                    value: "А",
                    style: letter_style,
                    method: HelperKeys.add,
                },
                {
                    position: {
                        x: scene_x_center - 300,
                        y: letter_y,
                    },
                    text: "Б",
                    value: "Б",
                    style: letter_style,
                    method: HelperKeys.add,
                },
                {
                    position: {
                        x: scene_x_center - 200,
                        y: letter_y,
                    },
                    text: "В",
                    value: "В",
                    style: letter_style,
                    method: HelperKeys.add,
                },

                {
                    position: {
                        x: scene_x_center - 100,
                        y: letter_y,
                    },
                    text: "Г",
                    value: "Г",
                    style: letter_style,
                    method: HelperKeys.add,
                },

                {
                    position: {
                        x: scene_x_center,
                        y: letter_y,
                    },
                    text: "Д",
                    value: "Д",
                    style: letter_style,
                    method: HelperKeys.add,
                },

                {
                    position: {
                        x: scene_x_center + 100,
                        y: letter_y,
                    },
                    text: "Е",
                    value: "Е",
                    style: letter_style,
                    method: HelperKeys.add,
                },

                {
                    position: {
                        x: scene_x_center + 200,
                        y: letter_y,
                    },
                    text: "Ё",
                    value: "Ё",
                    style: letter_style,
                    method: HelperKeys.add,
                },

                {
                    position: {
                        x: scene_x_center + 300,
                        y: letter_y,
                    },
                    text: "Ж",
                    value: "Ж",
                    style: letter_style,
                    method: HelperKeys.add,
                },

                {
                    position: {
                        x: scene_x_center + 400,
                        y: letter_y,
                    },
                    text: "З",
                    value: "З",
                    style: letter_style,
                    method: HelperKeys.add,
                },

                {
                    position: {
                        x: scene_x_center - 400,
                        y: letter_y + 100,
                    },
                    text: "И",
                    value: "И",
                    style: letter_style,
                    method: HelperKeys.add,
                },
                {
                    position: {
                        x: scene_x_center - 300,
                        y: letter_y + 100,
                    },
                    text: "Й",
                    value: "Й",
                    style: letter_style,
                    method: HelperKeys.add,
                },
                {
                    position: {
                        x: scene_x_center - 200,
                        y: letter_y + 100,
                    },
                    text: "К",
                    value: "К",
                    style: letter_style,
                    method: HelperKeys.add,
                },

                {
                    position: {
                        x: scene_x_center - 100,
                        y: letter_y + 100,
                    },
                    text: "Л",
                    value: "Л",
                    style: letter_style,
                    method: HelperKeys.add,
                },

                {
                    position: {
                        x: scene_x_center,
                        y: letter_y + 100,
                    },
                    text: "М",
                    value: "М",
                    style: letter_style,
                    method: HelperKeys.add,
                },

                {
                    position: {
                        x: scene_x_center + 100,
                        y: letter_y + 100,
                    },
                    text: "Н",
                    value: "Н",
                    style: letter_style,
                    method: HelperKeys.add,
                },

                {
                    position: {
                        x: scene_x_center + 200,
                        y: letter_y + 100,
                    },
                    text: "О",
                    value: "О",
                    style: letter_style,
                    method: HelperKeys.add,
                },

                {
                    position: {
                        x: scene_x_center + 300,
                        y: letter_y + 100,
                    },
                    text: "П",
                    value: "П",
                    style: letter_style,
                    method: HelperKeys.add,
                },

                {
                    position: {
                        x: scene_x_center + 400,
                        y: letter_y + 100,
                    },
                    text: "Р",
                    value: "Р",
                    style: letter_style,
                    method: HelperKeys.add,
                },

                {
                    position: {
                        x: scene_x_center - 400,
                        y: letter_y + 200,
                    },
                    text: "С",
                    value: "С",
                    style: letter_style,
                    method: HelperKeys.add,
                },
                {
                    position: {
                        x: scene_x_center - 300,
                        y: letter_y + 200,
                    },
                    text: "Т",
                    value: "Т",
                    style: letter_style,
                    method: HelperKeys.add,
                },
                {
                    position: {
                        x: scene_x_center - 200,
                        y: letter_y + 200,
                    },
                    text: "У",
                    value: "У",
                    style: letter_style,
                    method: HelperKeys.add,
                },

                {
                    position: {
                        x: scene_x_center - 100,
                        y: letter_y + 200,
                    },
                    text: "Ф",
                    value: "Ф",
                    style: letter_style,
                    method: HelperKeys.add,
                },

                {
                    position: {
                        x: scene_x_center,
                        y: letter_y + 200,
                    },
                    text: "Х",
                    value: "Х",
                    style: letter_style,
                    method: HelperKeys.add,
                },

                {
                    position: {
                        x: scene_x_center + 100,
                        y: letter_y + 200,
                    },
                    text: "Ц",
                    value: "Ц",
                    style: letter_style,
                    method: HelperKeys.add,
                },

                {
                    position: {
                        x: scene_x_center + 200,
                        y: letter_y + 200,
                    },
                    text: "Ч",
                    value: "Ч",
                    style: letter_style,
                    method: HelperKeys.add,
                },

                {
                    position: {
                        x: scene_x_center + 300,
                        y: letter_y + 200,
                    },
                    text: "Ш",
                    value: "Ш",
                    style: letter_style,
                    method: HelperKeys.add,
                },

                {
                    position: {
                        x: scene_x_center + 400,
                        y: letter_y + 200,
                    },
                    text: "Щ",
                    value: "Щ",
                    style: letter_style,
                    method: HelperKeys.add,
                },

                {
                    position: {
                        x: scene_x_center - 400,
                        y: letter_y + 300,
                    },
                    text: "Ъ",
                    value: "Ъ",
                    style: letter_style,
                    method: HelperKeys.add,
                },
                {
                    position: {
                        x: scene_x_center - 300,
                        y: letter_y + 300,
                    },
                    text: "Ы",
                    value: "Ы",
                    style: letter_style,
                    method: HelperKeys.add,
                },
                {
                    position: {
                        x: scene_x_center - 200,
                        y: letter_y + 300,
                    },
                    text: "Ь",
                    value: "Ь",
                    style: letter_style,
                    method: HelperKeys.add,
                },

                {
                    position: {
                        x: scene_x_center - 100,
                        y: letter_y + 300,
                    },
                    text: "Э",
                    value: "Э",
                    style: letter_style,
                    method: HelperKeys.add,
                },

                {
                    position: {
                        x: scene_x_center,
                        y: letter_y + 300,
                    },
                    text: "Ю",
                    value: "Ю",
                    style: letter_style,
                    method: HelperKeys.add,
                },

                {
                    position: {
                        x: scene_x_center + 100,
                        y: letter_y + 300,
                    },
                    text: "Я",
                    value: "Я",
                    style: letter_style,
                    method: HelperKeys.add,
                },

                {
                    position: {
                        x: scene_x_center + 200,
                        y: letter_y + 300,
                    },
                    text: "_",
                    value: " ",
                    style: letter_style,
                    method: HelperKeys.add,
                },

                {
                    position: {
                        x: scene_x_center + 400,
                        y: letter_y + 300,
                    },
                    text: "<",
                    style: letter_style,
                    method: HelperKeys.remove,
                    value: "",
                },
            ],
        };

        this.letters_table_input = new LettersTableInput(this, letters_table_input_object);

        const control_btns_style = {
            fontFamily: "interbold",
            fontSize: "28px",
            color: "#DDD",
            align: "center",
        };

        const return_to_main_menu_btn = this.add
            .text(this.scale.width / 2 + 320, 705, "ПРОДОЛЖИТЬ", control_btns_style)
            .setOrigin(0.5, 0.5)
            .setInteractive();

        this.setUpTextButtonsEvents(return_to_main_menu_btn);

        return_to_main_menu_btn.on("pointerup", function (pointer: any) {
            if (!context.letters_table_input.text_field.text.trim()) return;
            context.addNewRecordObject(context.registry.get(HelperKeys.score), context.letters_table_input.text_field.text.trim());
        });

        const full_screen_btn = new FullScreenBtn(this, {
            position: {
                x: this.scale.width - 100,
                y: 20,
            },
        });
    }
    
    private addNewRecordObject(score: string, name: string) {
        
    }

    private setUpTextButtonsEvents(button: Phaser.GameObjects.Text): void {
        button.on("pointerdown", function (pointer: any) {
            button.setColor("#FFF");
        });
        button.on("pointerout", function (pointer: any) {
            button.setColor("#DDD");
        });
        button.on("pointerup", function (pointer: any) {
            button.setColor("#DDD");
        });
    }

    update(t: number, dt: number): void {}
}
