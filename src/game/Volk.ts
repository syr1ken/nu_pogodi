import * as Phaser from 'phaser';

import TextureKeys from "../consts/TextureKeys";
import PositionKeys from "../consts/PositionKeys";
import { VolkObject } from "../consts/interfaces";
import Health from "../game/Health";

export default class Volk extends Phaser.GameObjects.Container {
    private cursors!: Phaser.Types.Input.Keyboard.CursorKeys;
    private volk_sprite!: Phaser.GameObjects.Sprite;
    public volk_position!: string;
    public health!: Health;
    public frames!: number;

    constructor(scene: Phaser.Scene, volk: VolkObject) {
        super(scene, volk.position.x, volk.position.y);
        this.volk_sprite = scene.physics.add.sprite(volk.position.x, volk.position.y, TextureKeys.volk, undefined);
        this.volk_position = PositionKeys.top_left;
        this.cursors = scene.input.keyboard!.createCursorKeys();
        this.health = new Health(scene, volk.health);
        this.frames = volk.frames;
    }

    public inputHandler(): void {
        if (this.cursors.left.isDown) {
            if (this.volk_position === PositionKeys.top_left || this.volk_position === PositionKeys.top_right) {
                this.turnTopLeft();
            }

            if (this.volk_position === PositionKeys.bottom_left || this.volk_position === PositionKeys.bottom_right) {
                this.turnBottomLeft();
            }
        } else if (this.cursors.right.isDown) {
            if (this.volk_position === PositionKeys.top_left || this.volk_position === PositionKeys.top_right) {
                this.turnTopRight();
            }

            if (this.volk_position === PositionKeys.bottom_left || this.volk_position === PositionKeys.bottom_right) {
                this.turnBottomRight();
            }
        } else if (this.cursors.up.isDown) {
            if (this.volk_position === PositionKeys.bottom_left || this.volk_position === PositionKeys.top_left) {
                this.turnTopLeft();
            }

            if (this.volk_position === PositionKeys.bottom_right || this.volk_position === PositionKeys.top_right) {
                this.turnTopRight();
            }
        } else if (this.cursors.down.isDown) {
            if (this.volk_position === PositionKeys.top_left || this.volk_position === PositionKeys.bottom_left) {
                this.turnBottomLeft();
            }

            if (this.volk_position === PositionKeys.top_right || this.volk_position === PositionKeys.bottom_right) {
                this.turnBottomRight();
            }
        }
    }

    public turnTopLeft() {
        this.volk_position = PositionKeys.top_left;
        this.volk_sprite.setFrame(0);
    }

    public turnBottomLeft() {
        this.volk_position = PositionKeys.bottom_left;
        this.volk_sprite.setFrame(1);
    }

    public turnTopRight() {
        this.volk_position = PositionKeys.top_right;
        this.volk_sprite.setFrame(2);
    }

    public turnBottomRight() {
        this.volk_position = PositionKeys.bottom_right;
        this.volk_sprite.setFrame(3);
    }

    public addHealth(value: number): void {
        this.health.current_health = this.health.current_health + value;
        this.health.current_frame = this.health.max_health - this.health.current_health;
        this.setHealthSpriteFrame();
    }

    public subtractHealth(value: number): void {
        this.health.current_health = this.health.current_health - value;
        this.health.current_frame = this.health.max_health - this.health.current_health;
        this.setHealthSpriteFrame();
    }

    private setHealthSpriteFrame(): void {
        if (this.health.current_frame > 0 && this.health.current_frame < this.health.frames) {
            this.health.health_sprite.setFrame(this.health.current_frame);
        }
    }

    public destroySprites(): void {
        this.health.destroy();
        this.volk_sprite.destroy();
    }
}
