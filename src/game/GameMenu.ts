import * as Phaser from "phaser";

import SceneKeys from "../consts/SceneKeys";
import { GameMenuObject } from "../consts/interfaces";

export default class GameMenu extends Phaser.GameObjects.Container {
    private resume_game_btn!: Phaser.GameObjects.Text;
    private start_new_game_btn!: Phaser.GameObjects.Text;
    private go_to_main_menu_btn!: Phaser.GameObjects.Text;
    private resume_game_container!: Phaser.GameObjects.Container;
    private start_new_game_container!: Phaser.GameObjects.Container;
    private go_to_main_menu_container!: Phaser.GameObjects.Container;
    public difficulty_text!: Phaser.GameObjects.Text;

    constructor(scene: Phaser.Scene, menu_overlay: GameMenuObject) {
        super(scene, menu_overlay.position.x, menu_overlay.position.y);

        this.resume_game_btn = scene.add
            .text(
                menu_overlay.resume_game_btn.position.x,
                menu_overlay.resume_game_btn.position.y,
                menu_overlay.resume_game_btn.text,
                menu_overlay.resume_game_btn.style.text_style
            )
            .setOrigin(0.5, 0.5)
            .setInteractive();

        this.resume_game_container = scene.add
            .container(menu_overlay.resume_game_btn.position.x, menu_overlay.resume_game_btn.position.y)
            .setSize(this.resume_game_btn.displayWidth, this.resume_game_btn.displayHeight)
            .setInteractive();

        this.setUpTextButtonsEvents(this.resume_game_container, this.resume_game_btn, {
            color: menu_overlay.resume_game_btn.style.color,
            hoverColor: menu_overlay.resume_game_btn.style.hoverColor,
            activeColor: menu_overlay.resume_game_btn.style.activeColor,
        });

        this.resume_game_container.on("pointerup", function (pointer: any) {
            scene.scene.stop(SceneKeys.GamePauseScene);
            scene.scene.bringToTop(SceneKeys.BackgroundScene);
            scene.scene.bringToTop(SceneKeys.GameScene);
            scene.scene.bringToTop(SceneKeys.UIScene);
            scene.scene.resume(SceneKeys.GameScene);
        });

        this.start_new_game_btn = scene.add
            .text(
                menu_overlay.start_new_game_btn.position.x,
                menu_overlay.start_new_game_btn.position.y,
                menu_overlay.start_new_game_btn.text,
                menu_overlay.start_new_game_btn.style.text_style
            )
            .setOrigin(0.5, 0.5)
            .setInteractive();

        this.start_new_game_container = scene.add
            .container(menu_overlay.start_new_game_btn.position.x, menu_overlay.start_new_game_btn.position.y)
            .setSize(this.start_new_game_btn.displayWidth, this.start_new_game_btn.displayHeight)
            .setInteractive();

        this.setUpTextButtonsEvents(this.start_new_game_container, this.start_new_game_btn, {
            color: menu_overlay.start_new_game_btn.style.color,
            hoverColor: menu_overlay.start_new_game_btn.style.hoverColor,
            activeColor: menu_overlay.start_new_game_btn.style.activeColor,
        });

        this.start_new_game_container.on("pointerup", function (pointer: any) {
            scene.scene.stop(SceneKeys.GameScene);
            scene.scene.start(SceneKeys.GameScene);
            scene.scene.bringToTop(SceneKeys.BackgroundScene);
            scene.scene.bringToTop(SceneKeys.GameScene);
            scene.scene.bringToTop(SceneKeys.UIScene);
        });

        this.go_to_main_menu_btn = scene.add
            .text(
                menu_overlay.go_to_main_menu_btn.position.x,
                menu_overlay.go_to_main_menu_btn.position.y,
                menu_overlay.go_to_main_menu_btn.text,
                menu_overlay.go_to_main_menu_btn.style.text_style
            )
            .setOrigin(0.5, 0.5)
            .setInteractive();

        this.go_to_main_menu_container = scene.add
            .container(menu_overlay.go_to_main_menu_btn.position.x, menu_overlay.go_to_main_menu_btn.position.y)
            .setSize(this.go_to_main_menu_btn.displayWidth, this.go_to_main_menu_btn.displayHeight)
            .setInteractive();

        this.setUpTextButtonsEvents(this.go_to_main_menu_container, this.go_to_main_menu_btn, {
            color: menu_overlay.go_to_main_menu_btn.style.color,
            hoverColor: menu_overlay.go_to_main_menu_btn.style.hoverColor,
            activeColor: menu_overlay.go_to_main_menu_btn.style.activeColor,
        });

        this.go_to_main_menu_container.on("pointerup", function (pointer: any) {
            scene.scene.stop(SceneKeys.GameScene);
            scene.scene.start(SceneKeys.StartGameMenuScene);
            scene.scene.bringToTop(SceneKeys.BackgroundScene);
            scene.scene.bringToTop(SceneKeys.StartGameMenuScene);
            scene.scene.bringToTop(SceneKeys.UIScene);
        });

        this.difficulty_text = scene.add
            .text(
                menu_overlay.difficulty_text.position.x,
                menu_overlay.difficulty_text.position.y,
                menu_overlay.difficulty_text.text,
                menu_overlay.difficulty_text.style.text_style
            )
            .setOrigin(0.5, 0.5)
            .setInteractive();
    }

    private setUpTextButtonsEvents(
        container: Phaser.GameObjects.Container,
        text: Phaser.GameObjects.Text,
        style: any
    ): void {
        container.on("pointerdown", function (pointer: any) {
            text.setColor(style.activeColor);
        });
        container.on("pointerout", function (pointer: any) {
            text.setColor(style.color);
        });
        container.on("pointerover", function (pointer: any) {
            text.setColor(style.hoverColor);
        });
    }

    public setVisibility(visible: boolean): void {
        this.resume_game_btn.visible = visible;
        this.difficulty_text.visible = visible;
        this.go_to_main_menu_btn.visible = visible;
        this.start_new_game_btn.visible = visible;
        this.resume_game_container.visible = visible;
        this.start_new_game_container.visible = visible;
        this.go_to_main_menu_container.visible = visible;
    }

    public destroy(): void {
        this.resume_game_btn.destroy();
        this.difficulty_text.destroy();
        this.go_to_main_menu_btn.destroy();
        this.start_new_game_btn.destroy();
        this.resume_game_container.destroy();
        this.start_new_game_container.destroy();
        this.go_to_main_menu_container.destroy();
    }
}
