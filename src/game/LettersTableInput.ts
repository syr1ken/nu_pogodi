import * as Phaser from "phaser";

import { LettersTableInputObject } from "../consts/interfaces";
import { HelperKeys } from "../consts/HelperKeys";

export default class LettersTableInput extends Phaser.GameObjects.Container {
    public text_field!: Phaser.GameObjects.Text;
    private text_buttons: Phaser.GameObjects.Text[] = [];
    private text_field_underline!: Phaser.GameObjects.Graphics;
    private text_field_underline_min_width: number = 20;

    constructor(scene: Phaser.Scene, letters_table_input: LettersTableInputObject) {
        super(scene, letters_table_input.position.x, letters_table_input.position.y);
        const context = this;

        this.text_field = scene.add
            .text(
                letters_table_input.text_field.position.x,
                letters_table_input.text_field.position.y,
                letters_table_input.text_field.text,
                letters_table_input.text_field.style
            )
            .setOrigin(0.5, 0.5);

        for (let letter_idx = 0; letter_idx < letters_table_input.letters_list.length; letter_idx++) {
            const button = scene.add
                .text(
                    letters_table_input.letters_list[letter_idx].position.x,
                    letters_table_input.letters_list[letter_idx].position.y,
                    letters_table_input.letters_list[letter_idx].text,
                    letters_table_input.letters_list[letter_idx].style
                )
                .setOrigin(0.5, 0.5)
                .setInteractive();

            button.on("pointerdown", function (pointer: any) {
                button.setScale(1.2, 1.2);
                const button_obj = letters_table_input.letters_list[letter_idx];

                if (button_obj.method === HelperKeys.add) {
                    context.addLetter(button_obj.value);
                } else if (button_obj.method === HelperKeys.remove) {
                    context.removeLetter();
                }

                if (context.text_field.displayWidth > context.text_field_underline_min_width) {
                    context.text_field_underline.clear();
                    context.text_field_underline.fillRect(
                        context.text_field.x - context.text_field.displayWidth / 2,
                        context.text_field.y + 20,
                        context.text_field.displayWidth,
                        5
                    );
                } else {
                    context.text_field_underline.clear();
                }
            });

            button.on("pointerup", function (pointer: any) {
                button.setScale(1.1, 1.1);
            });

            button.on("pointerover", function (pointer: any) {
                button.setScale(1.1, 1.1);
            });

            button.on("pointerout", function (pointer: any) {
                button.setScale(1, 1);
            });

            this.text_buttons.push(button);
        }

        this.text_field_underline = scene.add.graphics();

        this.text_field_underline.fillStyle(0xffffff);
    }

    addLetter(text: string): void {
        if (this.text_field.text.length < 35) {
            this.text_field.setText(this.text_field.text + text);
        }
    }

    removeLetter(): void {
        if (this.text_field.text.length) {
            this.text_field.setText(this.text_field.text.slice(0, -1));
        }
    }
}
