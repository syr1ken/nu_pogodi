import * as Phaser from 'phaser';

import TextureKeys from "../consts/TextureKeys";
import { ScreenOverlayObject } from "../consts/interfaces";

export default class ScreenOverlay extends Phaser.GameObjects.Container {
    private screen_overlay_sprite!: Phaser.GameObjects.Image;

    constructor(scene: Phaser.Scene, screen_overlay: ScreenOverlayObject) {
        super(scene, screen_overlay.position.x, screen_overlay.position.y);
        this.screen_overlay_sprite = scene.add.image(
            screen_overlay.position.x,
            screen_overlay.position.y,
            TextureKeys.screen_overlay
        ).setOrigin(0, 0);
    }

    public setVisibility(visible: boolean): void {
        this.screen_overlay_sprite.visible = visible;
    }

    public destroy(): void {
        this.screen_overlay_sprite.destroy();
    }
}
