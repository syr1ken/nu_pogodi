import * as Phaser from "phaser";

import TextureKeys from "../consts/TextureKeys";
import { VolumeBtnObject } from "../consts/interfaces";

export default class VolumeBtn extends Phaser.GameObjects.Container {
    private volume_sprite!: Phaser.GameObjects.Sprite;

    constructor(scene: Phaser.Scene, volume: VolumeBtnObject) {
        super(scene, volume.position.x, volume.position.y);
        const context = this;

        this.volume_sprite = scene.add
            .sprite(volume.position.x, volume.position.y, TextureKeys.volume_btn)
            .setOrigin(0, 0)
            .setInteractive();

        this.volume_sprite.on("pointerdown", function (pointer: any) {
            context.onVolumeClick(scene, context);
        });
    }

    onVolumeClick(scene: Phaser.Scene, context: any) {
        if (scene.game.sound.mute) {
            scene.game.sound.setMute(false);
            context.volume_sprite.setFrame(0);
        } else {
            context.volume_sprite.setFrame(1);
            scene.game.sound.setMute(true);
        }
    }

    public destroy(): void {
        this.volume_sprite.destroy();
    }
}
