import * as Phaser from "phaser";

import { GameOverObject } from "../consts/interfaces";
import SceneKeys from "../consts/SceneKeys";

export default class GameOver extends Phaser.GameObjects.Container {
    private start_new_game_btn!: Phaser.GameObjects.Text;
    private go_to_main_menu_btn!: Phaser.GameObjects.Text;
    private start_new_game_container!: Phaser.GameObjects.Container;
    private go_to_main_menu_container!: Phaser.GameObjects.Container;
    public score_text!: Phaser.GameObjects.Text;

    constructor(scene: Phaser.Scene, game_over: GameOverObject) {
        super(scene, game_over.position.x, game_over.position.y);

        this.score_text = scene.add
            .text(
                game_over.score_text.position.x,
                game_over.score_text.position.y,
                game_over.score_text.text,
                game_over.score_text.style.text_style
            )
            .setOrigin(0.5, 0.5)
            .setInteractive();

        this.start_new_game_btn = scene.add
            .text(
                game_over.start_new_game_btn.position.x,
                game_over.start_new_game_btn.position.y,
                game_over.start_new_game_btn.text,
                game_over.start_new_game_btn.style.text_style
            )
            .setOrigin(0.5, 0.5)
            .setInteractive();

        this.start_new_game_container = scene.add
            .container(game_over.start_new_game_btn.position.x, game_over.start_new_game_btn.position.y)
            .setSize(this.start_new_game_btn.displayWidth, this.start_new_game_btn.displayHeight)
            .setInteractive();

        this.setUpTextButtonsEvents(this.start_new_game_container, this.start_new_game_btn, {
            color: game_over.start_new_game_btn.style.color,
            hoverColor: game_over.start_new_game_btn.style.hoverColor,
            activeColor: game_over.start_new_game_btn.style.activeColor,
        });

        this.start_new_game_container.on("pointerup", function (pointer: any) {
            scene.scene.stop(SceneKeys.GameOverScene);
            scene.scene.start(SceneKeys.GameScene);
            scene.scene.bringToTop(SceneKeys.BackgroundScene);
            scene.scene.bringToTop(SceneKeys.GameScene);
            scene.scene.bringToTop(SceneKeys.UIScene);
        });

        this.go_to_main_menu_btn = scene.add
            .text(
                game_over.go_to_main_menu_btn.position.x,
                game_over.go_to_main_menu_btn.position.y,
                game_over.go_to_main_menu_btn.text,
                game_over.go_to_main_menu_btn.style.text_style
            )
            .setOrigin(0.5, 0.5)
            .setInteractive();

        this.go_to_main_menu_container = scene.add
            .container(game_over.go_to_main_menu_btn.position.x, game_over.go_to_main_menu_btn.position.y)
            .setSize(this.go_to_main_menu_btn.displayWidth, this.go_to_main_menu_btn.displayHeight)
            .setInteractive();

        this.setUpTextButtonsEvents(this.go_to_main_menu_container, this.go_to_main_menu_btn, {
            color: game_over.go_to_main_menu_btn.style.color,
            hoverColor: game_over.go_to_main_menu_btn.style.hoverColor,
            activeColor: game_over.go_to_main_menu_btn.style.activeColor,
        });

        this.go_to_main_menu_container.on("pointerup", function (pointer: any) {
            scene.scene.stop(SceneKeys.GameOverScene);
            scene.scene.start(SceneKeys.StartGameMenuScene);
            scene.scene.bringToTop(SceneKeys.BackgroundScene);
            scene.scene.bringToTop(SceneKeys.StartGameMenuScene);
            scene.scene.bringToTop(SceneKeys.UIScene);
        });
    }

    private setUpTextButtonsEvents(
        container: Phaser.GameObjects.Container,
        text: Phaser.GameObjects.Text,
        style: any
    ): void {
        container.on("pointerdown", function (pointer: any) {
            text.setColor(style.activeColor);
        });
        container.on("pointerout", function (pointer: any) {
            text.setColor(style.color);
        });
        container.on("pointerover", function (pointer: any) {
            text.setColor(style.hoverColor);
        });
    }

    public setVisibility(visible: boolean): void {
        this.score_text.visible = visible;
        this.start_new_game_btn.visible = visible;
        this.go_to_main_menu_btn.visible = visible;
        this.start_new_game_container.visible = visible;
        this.go_to_main_menu_container.visible = visible;
    }

    public destroy(): void {
        this.score_text.destroy();
        this.start_new_game_btn.destroy();
        this.go_to_main_menu_btn.destroy();
        this.start_new_game_container.destroy();
        this.go_to_main_menu_container.destroy();
    }
}
