import * as Phaser from "phaser";

import TextureKeys from "../consts/TextureKeys";
import { FullScreenBtnObject } from "../consts/interfaces";

export default class FullScreenBtn extends Phaser.GameObjects.Container {
    private full_screen_sprite!: Phaser.GameObjects.Sprite;

    constructor(scene: Phaser.Scene, full_screen: FullScreenBtnObject) {
        super(scene, full_screen.position.x, full_screen.position.y);
        const context = this;

        this.full_screen_sprite = scene.add
            .sprite(full_screen.position.x, full_screen.position.y, TextureKeys.full_screen_btn)
            .setOrigin(0, 0)
            .setInteractive();

        this.full_screen_sprite.on("pointerdown", function (pointer: any) {
            context.onFullScreenClicked(scene, context);
        });
    }

    onFullScreenClicked(scene: Phaser.Scene, context: any) {
        if (scene.scale.isFullscreen) {
            context.full_screen_sprite.setFrame(0);
            scene.scale.stopFullscreen();
        } else {
            context.full_screen_sprite.setFrame(1);
            scene.scale.startFullscreen();
        }
    }

    public destroy(): void {
        this.full_screen_sprite.destroy();
    }
}
