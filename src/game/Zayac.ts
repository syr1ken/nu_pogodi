import * as Phaser from 'phaser';

import TextureKeys from "../consts/TextureKeys";
import { ZayacObject } from "../consts/interfaces";

export default class Zayac extends Phaser.GameObjects.Container {
    public zayac_sprite!: Phaser.GameObjects.Sprite;
    private show_ticks: number = 0;

    constructor(scene: Phaser.Scene, zayac: ZayacObject) {
        super(scene, zayac.position.x, zayac.position.y);
        this.zayac_sprite = scene.add.sprite(zayac.position.x, zayac.position.y, TextureKeys.zayac);
        this.show_ticks = zayac.show_ticks;
    }

    public get getShowTicks(): number{
        return this.show_ticks;
    }

    public set setShowTicks(value: number){
        if(value > 0) {
            this.show_ticks = value;
        }
    }

    public subtractOneShowTick(): void {
        if(this.show_ticks > 0) {
            this.show_ticks--;
        }
    }

    public destroy(): void {
        this.zayac_sprite.destroy();
    }
}
