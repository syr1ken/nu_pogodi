import * as Phaser from 'phaser';

import { ScoreObject } from "../consts/interfaces";
import { padWithZeroes } from "../helpers";

export default class Score extends Phaser.GameObjects.Container {
    private score_text!: Phaser.GameObjects.Text;
    public score: number = 0;

    constructor(scene: Phaser.Scene, score: ScoreObject) {
        super(scene, score.position.x, score.position.y);
        this.score = score.score;
        this.score_text = scene.add.text(score.position.x, score.position.y, padWithZeroes(this.score, 4), score.style);
    }

    public destroy(): void {
        this.score_text.destroy();
    }

    public updateScoreText(): void {
        this.score_text.setText(padWithZeroes(this.score, 4));
    }
}
