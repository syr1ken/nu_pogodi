import * as Phaser from 'phaser';

import { ChickObject } from "../consts/interfaces";

export default class Egg extends Phaser.GameObjects.Container {
    private chick_sprite!: Phaser.GameObjects.Sprite;
    public frames!: number;
    public current_frame!: number;

    constructor(scene: Phaser.Scene, chick: ChickObject) {
        super(scene, chick.position.x, chick.position.y);
        this.chick_sprite = scene.add.sprite(chick.position.x, chick.position.y, chick.id);
        this.current_frame = chick.current_frame;
        this.frames = chick.frames;
    }

    public setNextFrame(): void {
        this.current_frame++;
        this.chick_sprite.setFrame(this.current_frame);
    }

    public destroy(): void {
        this.chick_sprite.destroy();
    }
}
