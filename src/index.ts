import * as Phaser from 'phaser';
import config from "./config";
import GameScene from "./scenes/GameScene";
import StartGameMenuScene from "./scenes/StartGameMenuScene";
import AddNewRecord from "./scenes/AddNewRecord";
import PreloaderScene from "./scenes/PreloaderScene";
import GamePauseScene from "./scenes/GamePauseScene";
import UIScene from './scenes/UIScene';
import BackgroundScene from './scenes/BackgroundScene';
import GameOverScene from './scenes/GameOverScene';

new Phaser.Game(
    Object.assign(config, {
        scene: [PreloaderScene, StartGameMenuScene, GamePauseScene, GameOverScene, GameScene, AddNewRecord, UIScene, BackgroundScene],
    })
);
