export function padWithZeroes(number: number, length: number): string {
    var my_string = "" + number;
    while (my_string.length < length) {
        my_string = "0" + my_string;
    }
    return my_string;
}

export function doesIdExist(id: number, list: any[]): boolean {
    const item = list.find((item): boolean => {
        return item.id === id;
    });

    return item ? true : false;
}
