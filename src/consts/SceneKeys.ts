enum SceneKeys {
    PreloaderScene = 'preloader_scene',
    GameScene = 'game_scene',
    StartGameMenuScene = "start_game_menu_scene",
    GamePauseScene = "game_pause_scene",
    AddNewRecord = "add_new_record",
    UIScene = "ui_scene",
    BackgroundScene = "background_scene",
    GameOverScene = "game_over_scene"
}

export default SceneKeys
