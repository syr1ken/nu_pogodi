enum PositionKeys {
    top = "top",
    bottom = "bottom",
    left = "left",
    right = "right",
    top_left = "top-left",
    top_right = "top_right",
    bottom_left = "bottom-left",
    bottom_right = "bottom-right",
}

export default PositionKeys;
