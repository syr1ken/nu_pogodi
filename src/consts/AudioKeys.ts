enum AudioKeys {
    egg_tick = "egg_tick",
    egg_broke = "egg_broke",
    egg_catch = "egg_catch",
    game_over = "game_over",
}

export default AudioKeys;
