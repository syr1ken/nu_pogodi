export enum HelperKeys {
    remove = "remove",
    add = "add",
    score = "score",
    init = "init",
    suspended = "suspended",
    running = "running",
    shutdown = "shutdown",
    preupdate = "preupdate",
}
