enum TextureKeys {
    background = "background",
    volk = "volk",
    zayac = "zayac",
    egg_top_left = "egg_top_left",
    egg_bottom_left = "egg_bottom_left",
    egg_top_right = "egg_top_right",
    egg_bottom_right = "egg_bottom_right",
    health = "health",
    broken_egg = "broken_egg",
    chick_left = "chick_left",
    chick_right = "chick_right",
    button = "button",
    long_button = "long_button",
    menu_overlay = "menu_overlay",
    screen_overlay = "screen_overlay",
    full_screen_btn = "full_screen_btn",
    volume_btn = "volume_btn"
}

export default TextureKeys;
